package org.vanted.addons.SBGN_microbe_interaction;

import de.ipk_gatersleben.ag_nw.graffiti.plugins.gui.webstart.Main;

public class StartVantedWithAddon {

	public static void main(String[] args) {
		Main.startVanted(args, getAddonName());
	}

	public static String getAddonName() {
		return "SBGN-microbe-interaction.xml";
	}

}
