package org.vanted.addons.SBGN_microbe_interaction.importer;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import de.ipk_gatersleben.ag_nw.graffiti.plugins.gui.layout_control.dbe.ExperimentDataFileReader;
import de.ipk_gatersleben.ag_nw.graffiti.plugins.gui.layout_control.dbe.ExperimentDataPresenter;
import de.ipk_gatersleben.ag_nw.graffiti.plugins.gui.layout_control.dbe.TableData;
import de.ipk_gatersleben.ag_nw.graffiti.plugins.gui.webstart.ExperimentDataDragAndDropHandler;

public class MIExperimentDataDragAndDropHandler implements ExperimentDataDragAndDropHandler {
	
	ExperimentDataPresenter dataPresenter;

	@Override
	public boolean process(List<File> files) {
		List<TableData> dataList = new LinkedList<TableData>();
		files.forEach(file -> dataList.add(ExperimentDataFileReader.getExcelTableData(file)));
		// TODO create ExperimentInterface object
		// TODO correct input parameters
		dataPresenter.processReceivedData(null, null, null, null);
		return false;
	}

	@Override
	public boolean canProcess(File f) {
		// TODO check file
		return true;
	}

	@Override
	public boolean hasPriority() {
		return true;
	}

	@Override
	public void setExperimentDataReceiver(ExperimentDataPresenter receiver) {
		dataPresenter = receiver;
	}
}
