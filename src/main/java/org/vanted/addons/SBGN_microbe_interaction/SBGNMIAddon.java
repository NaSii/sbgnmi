package org.vanted.addons.SBGN_microbe_interaction;

import org.vanted.addons.SBGN_microbe_interaction.importer.MIExperimentDataDragAndDropHandler;
import org.vanted.addons.SBGN_microbe_interaction.importer.MIExperimentDataPresenter;
import org.vanted.addons.SBGN_microbe_interaction.importer.MIExperimentDataProcessorActivityFlow;
import org.vanted.addons.SBGN_microbe_interaction.importer.MIExperimentDataProcessorEntityRelationship;
import org.vanted.addons.SBGN_microbe_interaction.importer.MIExperimentDataProcessorProcessDescription;

import de.ipk_gatersleben.ag_nw.graffiti.plugins.addons.AddonAdapter;
import de.ipk_gatersleben.ag_nw.graffiti.plugins.gui.layout_control.dbe.ExperimentDataProcessingManager;
import de.ipk_gatersleben.ag_nw.graffiti.plugins.gui.webstart.GravistoMainHelper;

public class SBGNMIAddon extends AddonAdapter {
	
	@Override
	protected void initializeAddon() {
		
		MIExperimentDataDragAndDropHandler dndHanlder = new MIExperimentDataDragAndDropHandler();
		dndHanlder.setExperimentDataReceiver(new MIExperimentDataPresenter());
		GravistoMainHelper.addDragAndDropHandler(dndHanlder);
		
		ExperimentDataProcessingManager.addExperimentDataProcessor(new MIExperimentDataProcessorActivityFlow());
		ExperimentDataProcessingManager.addExperimentDataProcessor(new MIExperimentDataProcessorEntityRelationship());
		ExperimentDataProcessingManager.addExperimentDataProcessor(new MIExperimentDataProcessorProcessDescription());
		
	}	
}
